"use strict";

const student = {
    name: null,
    lastName: null,
    tabel: {},
}

student.name = prompt("Please enter your name", '');
student.lastName = prompt("Please enter your last name", '');
let subject = "";
let score = 0;

while (subject != null) {
    subject = prompt("Please enter subject", "");
    if(subject == null) {
        break
    }
    score = prompt("Please enter an estimate", "");
    student.tabel[subject] = score;
}

let numOfBadScores = 0,
    averageScore = 0,
    sum = 0,
    numOfScores = 0;


for (const subject in student.tabel) {
    if(student.tabel[subject] < 4) {
        numOfBadScores++;
    }
    numOfScores++;
    sum += +student.tabel[subject];
    averageScore = sum / numOfScores;
}

console.log(student);

console.log(`Кількість поганих (менше 4) оцінок з предметів: ${numOfBadScores}`);
if(numOfBadScores == 0) {
    console.log("Студента переведено на наступний курс");
}

console.log(`Середній бал з предметів: ${averageScore}`);
if(averageScore > 7) {
    console.log("Студенту призначено стипендію");
}